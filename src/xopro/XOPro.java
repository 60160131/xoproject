/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xopro;

import java.util.Scanner;

/**
 *
 * @author ACER
 */
public class XOPro {
    static char table[][] = {{'-', '-', '-'},
                            {'-', '-', '-'},
                            {'-', '-', '-'}};
    static int row;
    static int col;
    static char player = 'X';
    static int turn = 0;
    
    public static void main(String[] args) {
        showHeader();
        while (true) {
            System.out.println("");
            showTable();
            showTurn();
            input(); // row col player
            if (checkWin()) {
                break;
            }
            switchPlayer();
        }
        showTable();
        showWin();
    }

    private static void showHeader() {
        System.out.print(" XO GAME ");
    }

    private static void showTable() {
       System.out.println("  1 2 3");
       for (int rowIn = 0; rowIn < table.length; rowIn++) {
           System.out.print(rowIn+1);
            for (int colIn = 0; colIn < table[rowIn].length; colIn++) {
                System.out.print(" " + table[rowIn][colIn]);
            }
            System.out.println();
        }
    }

    private static void showTurn() {
        System.out.println(player + " Turn");
    }

    private static void showWin() {
        if (isDraw()) {
            System.out.println("Draw!!");
        }else {
            System.out.println(player + " Win!!");
        }   
    }

    private static void input() {
        while (true) {
            try {
                Scanner scan = new Scanner(System.in);
                System.out.print("input row col : ");
                String ip = scan.nextLine();
                String str[] = ip.split(" ");
                if (str.length != 2) {
                    System.out.println("Error : input row col(Ex.1 1)");
                }
                row = Integer.parseInt(str[0]);
                col = Integer.parseInt(str[1]);
                if (row > 3 || row < 1 || col > 3 || col < 1) {
                    System.out.println("Error : input row col between 1-3");
                    continue;
                }
                if (!setTable()) {
                    System.out.println("Error : another row col ");
                    continue;
                }
                break;
            }catch (Exception e) {
                System.out.println("Error : input row col(Ex.1 1)"); 
                continue;
            }
        }
    }
    
    private static boolean setTable() { //Error false
        if (table[row-1][col-1] != '-') {
            return false;
        }
        table[row-1][col-1] = player;
        return true;
    }
    
    private static boolean checkWin() {
        if (checkRow()){
            return true;
        }
        if (checkCol()){
            return true;
        }
        if (checkOblique()){
            return true;
        }
        if (isDraw()){
            return true;
        }
        return false;
    }
    
    private static boolean checkRow(int rowIn) {
        for (int colIn = 0; colIn < table[rowIn].length; colIn++) {
            if (table[rowIn][colIn] != player)
                return false;
        } 
        return true;
    }
    
    private static boolean checkRow() {
        for (int rowIn = 0; rowIn < table.length; rowIn++) {
            if (checkRow(rowIn))
                return true;
        }
        return false;
    }
    
    private static boolean checkCol(int colIn) {
        for (int rowIn = 0; rowIn < table[colIn].length; rowIn++) {
            if (table[rowIn][colIn] != player)
                return false;
        } 
        return true;
    } 
    
    private static boolean checkCol() {
        for (int colIn = 0; colIn < table[0].length; colIn++) {
            if (checkCol(colIn))
                return true;
        }
        return false;
    }
    
    private static boolean checkOblique() {
        if (checkoblique1()){
            return true;
        }
        if (checkoblique2()){
            return true;
        }
        return false;
    }
    
    private static boolean checkoblique1() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != player)
                return false;
        }
        return true;
    }
    
    private static boolean checkoblique2() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][2-i] != player)
                return false;
        }
        return true;
    }

    private static boolean isDraw() {
        if (turn == 8)
            return true;
        return false;
    }
    
    private static void switchPlayer() {
        player = player == 'X'?'O':'X';   
        turn++;
    }



    

    

    
    
}
